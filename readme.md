 # Installazione:

 mettere munge.php in una cartella qualsiasi
 fare in modo che la struttura sia: `compiled` e `website`
 
 - /
 	- munge.php
 	- /compiled
 	- /website
 
 in compiled ci possono anche essere altri file, tipo quelli compilati da grunt, gulp,
 qualunque cosa
 
 website, invece è in ascolto da PHP.
 
 entrambe possono essere cambiate in $base_config o a mano.
 
 si esegue semplicemente andando col terminale nella cartella
 e lanciando `php munge.php` (su unix e mac os x..) per windows credo vada messo il path
 al bin/php.. ma non so :D
 
 > Federico Quagliotto
 > 19 Giugno 2016