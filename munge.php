<?php

/*
 *	Installazione:
 *	mettere munge.php in una cartella qualsiasi
 *	fare in modo che la struttura sia: `compiled` e `website`
 *
 *	- /
 *		- munge.php
 *		- /compiled
 *		- /website
 *
 *	in compiled ci possono anche essere altri file, tipo quelli compilati da grunt, gulp,
 *	qualunque cosa
 *
 *	website, invece è in ascolto da PHP.
 *
 *	entrambe possono essere cambiate in $base_config o a mano.
 *
 *	si esegue semplicemente andando col terminale nella cartella
 *	e lanciando `php munge.php` (su unix e mac os x..) per windows credo vada messo il path
 *	al bin/php.. ma non so :D
 *
 *
 *	Federico Quagliotto
 *	19 Giugno 2016
 */

/*
 *	Configurazione base, verrà eseguita se non c'è sovrascrittura da cli.
 *	si può sovrascrivere appendendo in chiamata
 *	php munge.php directory=altra otput...
 *	CIAO
 *
 */

$base_config = [
	/* I file che osserverà per trovare modifiche */
	'directory' => 'website',

	/* Dove verranno salvati i file compilati */
	'output_directory' => 'compiled',

	/* File che vogliamo far compilare ['da compilare.php' => 'compilato.html'] */
	'output_file' => [
		'index.php' => 'index.html',
		'single.php' => 'single.html'
	],

	/* Tempo di attesa prima di cercare nuove modifiche - Impostare a -1 se non si vuole attivare un watcher */
	'sleepTime' => 2,

	/* Confirm Request */
	'confirm' => true,
];

/*
 *	Funzione di JOB,
 *	viene eseguita ogni volta un file viene modificato..
 *	è espandibile, di concetto.. io qui richiamo solo il file .php :) secco e semplice
 */

function job() {

	global $config, $counter;

	echo tl('JOB STARTED ('.$counter.' - '.@date('r').')');


	$file_da_includere_e_salvare = $config['output_file'];

	// Registro una funzione anonima, per comodità
	$scope = function($file) {

		if(is_file($file)) {
			ob_start();
			include $file;
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		} else {
			return ['error' => 1, 'msg' => $file . ' not found'];
		}

	};

	foreach($file_da_includere_e_salvare as $file_origin => $file_compiled) {
		$content = $scope($config['directory'] . DIRECTORY_SEPARATOR . $file_origin);
		if(!is_array($content)) {
			file_put_contents($config['output_directory'] . DIRECTORY_SEPARATOR . $file_compiled, $content);
			echo sc($file_compiled . ' compiled!');
		} else {
			echo tr($content['msg']);
		}
	}


	echo tl('JOB ENDED ('.$counter++.' - '.@date('r').')');

}


# # #


/*
 *	Avvio dell'applicazione.
 *	Da qui in poi può anche non interessare..
 */

/*
 *	Piccole Sistemazioni alla chiamata, unione del config passato dall'utente con il generale
 */

$root_directory = str_replace(basename(__file__), '', __file__);
$counter = 0;

unset($_SERVER['argv'][0]);
parse_str(implode('&', $_SERVER['argv']), $user_config);

$config = array_replace_recursive($base_config, $user_config);

/* Avvio dell'applicazione */

in();

if(count($user_config) > 0) {

	echo tl('User Configuration', 'Data provided from cli.');
	print_r($user_config);

	echo hr();
}

echo tl('Watching Configuration');
print_r($config);

if(($typed = prompt('Confirm?', ['y', 'n'])) == 'y' || $config['confirm'] == false) {

	calculate_hash($config['directory'], [], $starting_hashs);
	job();

	while(true) {

		$check = $starting_hashs;
		if(calculate_hash($config['directory'], $check, $starting_hashs)) {
			job();
		}

		if($config['sleepTime'] == -1)
			break;
		
		sleep($config['sleepTime']);

	}

} else {
	echo 'You have aborted the request, typing "'.$typed.'" instead of y :)';
}


echo "\n\n";

/*
 *	Functions
 */
function calculate_hash($directory, $origin, &$return) {

	$out = directory_tree($directory);
	$files = $out['files'];
	$trig = false;
	foreach($files as $file) {
		if($origin[$file['path']] != $file['hash_file']) {
			$trig = true;
		}
		$return[$file['path']] = $file['hash_file'];
	}

	return $trig;

}

function prompt($question, $options = false) {

	echo tl($question);
	echo 'Type your reply '.(count($options) > 0 ? '('.implode(', ', $options).')' : '' ).': ';
	$handle = fopen ("php://stdin","r");
	$line = fgets($handle);
	$reply = trim($line);
	fclose($handle);

	return $reply;

}

function in() {
	echo "\n\n";
	echo "munge.php - v 0.1" . "\n";
	echo c_text(33, "Federico Quagliotto (f.quagliotto@mandarinoadv.com)") . "\n\n";
}

function hr() {
	echo "\n" . '---' . "\n";
}

function tr($content) {
	echo c_text(31, 'ERR: ') . $content . "\n";
}

function c_text($color, $text) {
	return "\033[".$color."m".$text."\033[0m";
}

function sc($content) {
	echo c_text(32, 'SUCC: ') . $content . "\n";
}

function tl($title, $info = false) {

	$out[] = "\n";
	$out[] = c_text(33, str_pad('-', strlen($title), '-'));
	$out[] = c_text(35, strtoupper($title));
	$out[] = c_text(33, str_pad('-', strlen($title), '-'));
	$out[] = "\n";

	return implode("\n", $out);

}

function directory_tree($directory) {

	$path = $directory;
	
	$iter = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS),
		RecursiveIteratorIterator::SELF_FIRST,
		RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
	);
	
	foreach ($iter as $path => $dir) {
		if($dir->isDir())
			$paths[] = $path;
		else 
			$files[$path] = [ 
				'path' => $path, 
				'extension' => pathinfo($path, PATHINFO_EXTENSION),
				'hash_file' => md5_file($path),
				'filesize' => filesize($path),
				'creationTime' => filectime($path),
				'updateTime' => filemtime($path)
			];
	}
	
	return['files' => $files, 'folders' => $paths];

}